<script lang="ts" setup>
import { computed, nextTick, onMounted, ref } from 'vue';
import { Cell } from '../models/cell';
import { Cursor, CursorDirection } from '../models/cursor';
import { Grid } from '../models/grid';
import { Puzzle } from '../models/puzzle';
import LogoView from './LogoView.vue';
import AppDialog from './AppDialog.vue';
import PuzzleInfo from './PuzzleInfo.vue';
import { alwaysAllowEditing } from '../models/settings';
import { useQuasar } from 'quasar';
const props = defineProps<{
  grid: Grid;
  puzzle: Puzzle;
  cursor: Cursor;
  disableClose: boolean;
  vertical?: boolean;
}>();
const emit = defineEmits<{
  (event: 'close'): void;
}>();
function close() {
  emit('close');
}
function save() {
  props.puzzle.save();
}
function withCells(target: 'current' | 'selection' | 'all', method: 'check' | 'reveal') {
  let cells: Iterable<Cell> = [];
  if(target === 'current') {
    const cell = props.grid.game.cells.selected();
    if(cell) {
      cells = [cell]
    }
  } else if(target === 'selection') {
    cells = (props.grid.game.rows.selected() || props.grid.game.blooms.selected())?.cells || [];
  } else if(target === 'all') {
    cells = props.grid.game.cells;
  }
  for(const cell of cells) {
    cell[method]();
  }
}
const cursorButtonProps = computed(() =>
  props.cursor.mode.value === 'bloom'
    ? props.cursor.direction.value === CursorDirection.forward ? {
      icon: 'redo',
      label: 'Clockwise'
    } : {
      icon: 'undo',
      label: 'Counter-Clockwise',
    }
    : props.cursor.direction.value === CursorDirection.forward ? {
      icon: 'east',
      label: 'Left-to-right',
    }: {
      icon: 'west',
      label: 'Right-aligned',
    }
)
const info = ref<InstanceType<typeof AppDialog>>();
const quasar = useQuasar();
function setEditing(editing: boolean) {
  if (editing && !props.puzzle.draft.value) {
    quasar.dialog({
      title: 'Edit mode',
      message: 'Are you sure you want to enter edit mode? This will reveal the puzzle answers. If you are not the original author, please respect their copyrights.',
      cancel: true,
      ok: true,
      persistent: true,
    }).onOk(() => {
      props.puzzle.editing.value = editing;
    })
  } else {
    props.puzzle.editing.value = editing;
  }
}
const canonicalUrl = import.meta.env.VITE_CANONICAL_URL;
const appTitle = import.meta.env.VITE_APP_TITLE;
const showedInfo = ref(false)
function showInfo() {
  info.value?.show()
  showedInfo.value = true
}
onMounted(() => {
  if(!props.puzzle.editing.value && props.puzzle.notes.value) {
    nextTick(() => {
      showInfo()
    })
  }
})
defineExpose({
  showInfo,
})
</script>

<template lang="pug">
QToolbar.toolbar(:class="{vertical}")
  a.logo(
    v-if="disableClose"
    target="_blank"
    :title="appTitle"
    :href="canonicalUrl"
  )
    LogoView(text medium)
  QBtn(
    v-else
    @click="close"
    label="Close"
    title="Close puzzle and return to the start page"
    icon="logout"
    dense flat no-caps
  )
  template(v-if="puzzle.editing.value")
    QBtn(
      @click="save"
      label="Save"
      icon="save"
      title="Save the puzzle"
      dense flat no-caps
    )
  template(v-else)
    QBtnDropdown(
      @click="withCells('current', 'check')"
      label="Check"
      title="Check current cell for errors"
      icon="check"
      split dense flat no-caps
    )
      QList(dense)
        QItem(
          @click="withCells('selection', 'check')"
          title="Check selected cells for errors"
          clickable v-close-popup dense
        )
          QItemSection
            QItemLabel Selection
        QItem(
          title="Check all cells for errors"
          @click="withCells('all', 'check')"
          clickable v-close-popup dense
        )
          QItemSection
            QItemLabel All
    QBtnDropdown(
      @click="withCells('current', 'reveal')"
      label="Reveal"
      title="Reveal correct answer for current cell"
      icon="visibility"
      split dense flat no-caps
    )
      QList(dense)
        QItem(
          @click="withCells('selection', 'reveal')"
          title="Reveal correct answer for selected cells"
          clickable v-close-popup dense
        )
          QItemSection
            QItemLabel Selection
        QItem(
          title="Reveal correct answer for all cells"
          @click="withCells('all', 'reveal')"
          clickable v-close-popup dense
        )
          QItemSection
            QItemLabel All
  QBtn(
    v-bind="cursorButtonProps"
    title="Typing direction"
    @click="() => cursor?.cycle()"
    dense flat no-caps
  )
  QBtn(
    @click="showInfo()"
    title="Puzzle information"
    icon="info"
    label="Info"
    dense flat no-caps
  )
  AppDialog(ref="info")
    template(#title) Puzzle Info
    PuzzleInfo(
      :puzzle="puzzle"
      full
    )
  slot(name="app-toolbar-buttons")
  QBtnToggle(
    v-if="puzzle.editing.value || puzzle.draft.value || alwaysAllowEditing"
    :model-value="puzzle.editing.value"
    @update:model-value="setEditing"
    :options=`[
      {label: 'Play', value: false, icon: 'play_arrow'},
      {label: 'Edit', value: true, icon: 'edit'},
    ]`
    toggle-color="primary"
    dense flat no-caps
  )
</template>

<style scoped>
.toolbar {
  padding: 0;
  min-height: auto;
  flex-wrap: wrap;
  gap: 0.5em;
}
.toolbar.vertical {
  display: flex;
  flex-direction: column;
  align-items: flex-start;
}
.toolbar:not(.vertical) {
  height: var(--toolbar-height);
  container-type: inline-size;
}
@container (width < 800px) {
  .toolbar:not(.vertical) :deep(.q-btn__content i) {
    margin-right: 0;
  }
  .toolbar:not(.vertical) :deep(.q-btn__content .block) {
    display: none !important;
  }
}
.toolbar:not(.vertical) .logo {
  height: 110%;
}
.toolbar.vertical .logo {
  width: 100%;
}
@media screen and (width < 1000px) {
  .toolbar {
    gap: 0.25em;
  }
}
@media screen and (height < 700px) {
  .toolbar:not(.vertical) :deep(.q-btn__content .block) {
    display: none !important;
  }
}
.toolbar :deep(.q-btn__content) {
  flex-wrap: nowrap;
  white-space: pre;
}
</style>