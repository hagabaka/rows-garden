declare module 'svg-crowbar' {
  export interface Options {
    css?: 'inline' | 'internal' | 'none';
    downloadPNGOptions: {
      scale: number;
    }
  }

  export default function downloadSvg(element: SVGElement, filename?: string, options?: Options): void;

  export function downloadPng(element: SVGElement, filename?: string, options?: Options): void;
}