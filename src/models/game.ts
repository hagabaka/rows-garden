import { BloomSignature, Bloom } from "./bloom";
import { CellSignature, Cell } from "./cell";
import { BaseEntryProperties, BloomEntry, BloomEntrySignature, isBloom, isRow, RowEntry, RowEntrySignature } from "./entry";
import { Registry, Selection } from "./registry";
import { RowSignature, Row } from "./row";
import { Polygon } from "@mathigon/euclid";
import { Palette } from "./color";
import { Dimension, dimensionFromPuzzle } from "./dimension";
import { Grid, GridState } from "./grid";
import { Puzzle, PuzzleProperties } from "./puzzle";
import { Cursor, CursorDirection } from "./cursor";
import { shallowRef, watch } from "vue";
import { bloomClueOrdering } from "./settings";
import { autoSaveState } from "./session";

export interface GameElement {
  game: Game;
}

interface RegistryItemConstructor<Options, Item> {
  new(options: Options & GameElement): Item;
}

interface GameOptions {
  puzzleProperties: PuzzleProperties;
  cellRadius?: number;
  palette?: Palette;
  gridState?: GridState;
  dimension?: Dimension;
}

export class Game {
  cellRadius: number;
  palette: Palette;
  #grid = shallowRef<Grid>();
  #puzzle = shallowRef<Puzzle>();
  #cursor = shallowRef<Cursor>();
  #dimension = shallowRef<Dimension>();

  cells = new Registry<CellSignature, Cell>(
    ({columnIndex, rowIndex}) => `${columnIndex},${rowIndex}`,
    this.registryItemCreator(Cell),
  );
  rows = new Registry<RowSignature, Row>(
    ({index}) => `${index}`,
    this.registryItemCreator(Row),
  );
  blooms = new Registry<BloomSignature, Bloom>(
    ({color, index}) => `${color}-${index}`,
    this.registryItemCreator(Bloom),
  );
  entries = new Registry<BloomEntrySignature | RowEntrySignature, BloomEntry | RowEntry, (BloomEntrySignature | RowEntrySignature) & BaseEntryProperties>(
    signature => isRow(signature) ? `${signature.rowIndex}-${signature.index}` : `${signature.color}-${signature.index}`,
    properties => isRow(properties)
      ? new RowEntry({...properties, game: this})
      : new BloomEntry({...properties, game: this})
  );
  cluesSelection = new Selection(this.entries);

  constructor({
    cellRadius = 40,
    palette = {
      light: 'var(--light)',
      medium: 'var(--medium)',
      dark: 'var(--dark)',
    },
    puzzleProperties,
    gridState,
  }: GameOptions) {
    this.cellRadius = cellRadius;
    this.palette = palette;
    this.#dimension.value = puzzleProperties.dimension || dimensionFromPuzzle(puzzleProperties);
    this.#puzzle.value = new Puzzle({...puzzleProperties, game: this});

    this.#grid.value = new Grid({ game: this, state: gridState });
    this.#cursor.value = new Cursor(this);
    autoSaveState(this.#grid, this.#puzzle);
    watch(() => this.entries.selected(), (selected) => {
      if (this.bloomOrderIsSecret && selected) {
        this.cluesSelection.deselect()
      }
    })
    let autoMoveCursor = true
    watch(() => this.cluesSelection.selected(), (selected) => {
      if (this.bloomOrderIsSecret && selected) {
        autoMoveCursor = false
        this.entries.deselect()
        this.rows.deselect()
        this.blooms.deselect()
        if (isBloom(selected)) {
          const selectedCell = this.cells.selected();
          if (selectedCell && ![...this.blooms]
            .filter((bloom) => bloom.color === selected.color)
            .flatMap((bloom) => bloom.cells)
            .includes(selectedCell)
          ) {
            this.cells.deselect()
          }
        }
        autoMoveCursor = true
      }
    })
    watch([() => this.cells.selected()], ([selectedCell]) => {
      if (this.isOpen() && selectedCell && autoMoveCursor) {
        const { row, bloom } = selectedCell
        const selectedEntry = this.entries.selected()
        if (!selectedEntry || !selectedEntry.possibleCells.includes(selectedCell)) {
          if (this.cursor.mode.value === 'bloom') {
            this.blooms.select(bloom)
          } else {
            row.entries.find((entry) => entry.possibleCells.includes(selectedCell))?.select()
          }
        }
      }
    })
    watch(() => this.entries.selected(), (selected) => {
      if (selected) {
        if (isBloom(selected)) {
          this.rows.deselect()
        } else if (isRow(selected)) {
          this.blooms.deselect()
        }
      }
    })
    watch(() => this.rows.selected(), (selected) => {
      if (selected) {
        const selectedEntry = this.entries.selected()
        if(selectedEntry && isBloom(selectedEntry)) {
          this.entries.deselect()
        }
      }
    })
    watch(() => this.blooms.selected(), (selected) => {
      if (selected) {
        const selectedEntry = this.entries.selected()
        if(selectedEntry && isRow(selectedEntry)) {
          this.entries.deselect()
        }
        const selectedCell = this.cells.selected()
        if(selectedCell && !selected.cells.includes(selectedCell)) {
          this.cells.select(selected.cells[0])
        }
      }
    })
  }

  get grid() {
    return this.#grid.value
  }
  get puzzle() {
    return this.#puzzle.value
  }
  get cursor() {
    return this.#cursor.value
  }
  get dimension(): Dimension {
    if (!this.#dimension.value) {
      throw new Error('Dimension not set')
    }
    return this.#dimension.value
  }
  
  get cellTriangle() {
    return Polygon.regular(3, this.cellRadius);
  }

  get cellWidth() {
    return this.cellTriangle.edges[0].length;
  }

  get cellHeight() {
    return 2 * this.cellTriangle.area / this.cellWidth;
  }

  get rowLabelRadius(): number {
    return this.cellWidth * 0.25;
  }

  get rowRuleHeight(): number {
    return this.cellHeight * 0.2;
  }

  get gridWidth(): number {
    return (this.dimension.columnsPerRow + 1) * this.cellWidth / 2;
  }

  get gridHeight(): number {
    return this.dimension.rowsPerColumn * this.cellHeight;
  }

  get bloomOrderIsSecret() {
    return bloomClueOrdering.value !== 'gridOrder' && !this.isEditing
  }

  close() {
    this.#grid.value = undefined;
    this.#puzzle.value = undefined;
    this.#cursor.value = undefined;
    this.#dimension.value = undefined;
  }

  isOpen(): this is {
    grid: Grid
    puzzle: Puzzle
    cursor: Puzzle
   } {
    return !!this.grid &&
      !!this.puzzle &&
      !!this.cursor
  }

  get isEditing() {
    return !!this.#puzzle.value?.editing.value;
  }

  get status() {
    return this.#grid.value?.status.value;
  }

  registryItemCreator<Signature, Item, Properties = Signature>(
    itemContructor: RegistryItemConstructor<Properties, Item>,
  ): (properties: Properties) => Item {
    return (options => new itemContructor({...options, game: this}));
  }
}