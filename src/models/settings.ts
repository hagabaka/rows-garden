import { useStorage } from "@vueuse/core";
import { QVueGlobals } from "quasar";
import { watch } from "vue";
import { Bloom } from "./bloom";

export const darkMode = useStorage('darkMode', false);

export function useAutoDarkMode(quasar: QVueGlobals) {
  watch(darkMode, darkMode => {
    quasar.dark.set(darkMode);
  }, {
    immediate: true,
  });
  watch(() => quasar.dark.isActive, value => {
    darkMode.value = value;
  }, {
    immediate: true,
  });
}

export const skipFilledCells = useStorage('skipFilledCells', false);

export type EnumerationType = 'wordCount' | 'letterCount' | false;
export const enumerations = useStorage<EnumerationType>('enumerations', false);

export const alwaysAllowEditing = useStorage<boolean>('alwaysAllowEditing', false);

export const bloomClueOrders = ['gridOrder', 'answerOrder', 'random'] as const;
export type BloomClueOrderingType = typeof bloomClueOrders[number];
export const bloomClueOrdering = useStorage<BloomClueOrderingType>('bloomClueOrdering', 'random');
export const clueTextSize = useStorage<number>('clueTextSize', 16);
export const disabledTips = useStorage<Record<string, boolean>>('tips', {});

export const difficulties = ['easiest', 'easier', 'regular', 'harder', 'hardest', 'custom'] as const;
export type DifficultyType = typeof difficulties[number]
export const difficulty = useStorage<DifficultyType>('difficulty', 'regular')
export const difficultyDefinitions: Record<Exclude<DifficultyType, 'custom'>, {
  enumerations: EnumerationType
  bloomClueOrdering: BloomClueOrderingType
}> = {
  easiest: {
    enumerations: 'letterCount',
    bloomClueOrdering: 'gridOrder',
  },
  easier: {
    enumerations: 'wordCount',
    bloomClueOrdering: 'gridOrder',
  },
  regular: {
    enumerations: 'wordCount',
    bloomClueOrdering: 'answerOrder',
  },
  harder: {
    enumerations: 'wordCount',
    bloomClueOrdering: 'random',
  },
  hardest: {
    enumerations: false,
    bloomClueOrdering: 'random',
  },
}
export const blinkCursor = useStorage<boolean>('blinkCursor', true)