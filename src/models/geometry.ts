export const orientations = ['down', 'up'] as const;
export type Orientation = typeof orientations[number];
