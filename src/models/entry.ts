import { ref, Ref, watch } from "vue";
import { Bloom } from "./bloom";
import { Cell } from "./cell";
import { Color } from "./color";
import { Game, GameElement } from "./game";
import { Row } from "./row";
import { bloomClueOrdering, enumerations } from "./settings";
import range from "just-range";
import { getAdjacentCell } from "./cursor";

export interface BaseEntryProperties {
  clue: Ref<string>;
  answer: Ref<string>;
  index: number;
  game: Game;
}

class BaseEntry implements BaseEntryProperties, GameElement {
  clue: Ref<string>;
  answer: Ref<string>;
  filled: Ref<boolean>;
  index: number;
  note = ref<string>();
  game: Game;

  constructor({ clue, answer, index, game }: BaseEntryProperties) {
    this.clue = ref(clue);
    this.answer = ref(answer);
    this.filled = ref(false);
    this.index = index;
    this.game = game;
  }

  get number() {
    return this.index + 1;
  }

  get clueHtml() {
    return this.clue.value.replace(/\*([^*]+)\*/g, '<i>$1</i>');
  }

  get wordCount() {
    return this.answer.value.split(/\s+/i).length;
  }

  get answerLetters() {
    return answerLetters(this.answer.value)
  }

  get enumeration() {
    if (enumerations.value === 'wordCount') {
      const wordCount = this.answer.value.split(/\s+/i).length;
      return [
        ...wordCount > 1 ? [`${wordCount} words`] : [],
        ...this.answer.value.includes('-') ? ['with hyphen'] : [],
      ].join(', ')
    } else if (enumerations.value === 'letterCount') {
      return `(${this.answer.value
          .replaceAll(/[^-\sa-z]/ig, '')
          .replaceAll(/\s+/g, ',')
          .replaceAll(/[a-z]+/ig, (m) => m.length.toString())
          .replaceAll(/^,|,$/g, '')
        })`;
    } else {
      return '';
    }
  }

  get cells(): Cell[] {
    return []
  }

  get possibleCells(): Cell[] {
    return [...this.game.cells]
  }

  get isPossiblyFilled(): boolean {
    return this.possibleCells.every((cell) => !!cell.text.value)
  }

  get isOrderSecret(): boolean {
    return false;
  }

  get isSelected(): boolean {
    return (isRow(this) || isBloom(this)) && !!this.game.entries.isSelected(this)
  }

  get isPossiblySelected(): boolean {
    const cursor = this.game.cursor
    return !!cursor?.currentCell.value &&
      this.cells.includes(cursor.currentCell.value)
  }

  autoMarkFilled() {
    const stopWatch = watch(() => this.isPossiblyFilled, (isPossiblyFilled) => {
      if (isPossiblyFilled) {
        this.filled.value = true
      }
    })
    watch(() => this.game.isOpen(), (isOpen) => {
      if (!isOpen) {
        stopWatch();
      }
    });
  }
}

export interface RowEntrySignature {
  rowIndex: number;
  index: number;
}

export interface BloomEntrySignature {
  color: Color;
  index: number;
}

export function isRow(signature: RowEntrySignature | BloomEntrySignature | BaseEntry): signature is RowEntrySignature {
  return 'rowIndex' in signature;
}

export function isBloom(signature: RowEntrySignature | BloomEntrySignature | BaseEntry): signature is BloomEntrySignature {
  return 'color' in signature;
}

export class RowEntry extends BaseEntry implements RowEntrySignature {
  rowIndex: number;

  constructor({rowIndex, ...properties}: RowEntrySignature & BaseEntryProperties) {
    super(properties);
    this.rowIndex = rowIndex,
    watch(this.answer, () => {
      this.copyAnswerToGrid()
    })
    this.autoMarkFilled()
  }
  
  get row(): Row {
    return this.game.rows.getOrCreate({index: this.rowIndex});
  }

  get label() {
    return `${this.row.label}${this.row.entries.length > 1 ? this.number : ''}`;
  }

  get cells(): Cell[] {
    if (this.index === 0) {
      return this.row.cells.slice(0, this.answerLetters.length);
    } else {
      const cells = this.row.entries.slice(0, this.index).flatMap((entry) => entry.cells);
      return this.row.cells.slice(cells.length, cells.length + this.answerLetters.length);
    }
  }

  get possibleCells() {
    if (enumerations.value === 'letterCount') {
      return this.cells;
    }
    return this.row.cells.slice(
      this.isWholeRow || this.isAtStart ? 0 : 3,
      this.isWholeRow || this.isAtEnd ? undefined: -3,
    );
  }

  select() {
    this.game.entries.select(this);
    this.game.blooms.deselect();
    ensureSelectedCellContained(this, (selectedCell, contained) => {
      if(contained) {
        const lastCellIndex = this.row.cells.length - 1;
        if(this.isAtEnd && selectedCell?.columnIndex === 0) {
          return -1;
        } else if(this.isAtStart && !this.isWholeRow && selectedCell?.columnIndex === lastCellIndex) {
          return 0;
        }
      } else {
        return this.isAtEnd ? -1 : 0;
      }
    })
  }

  copyAnswerToGrid() {
    if (this.isAtStart) {
      for (const [index, letter] of this.answerLetters.entries()
      ) {
        this.cells[index].setText(letter)
      }
    } else if (this.isAtEnd) {
      for (const [index, letter] of this.answerLetters.entries()
      ) {
        this.cells[this.cells.length - this.answerLetters.length + index].setText(letter)
      }
    }
  }

  get gridMatchesAnswer() {
    if (this.isAtStart) {
      return this.answerLetters.every((letter, index) => this.cells[index].text.value === letter);
    } else if (this.isAtEnd) {
      return this.answerLetters.every((letter, index) => this.cells[this.cells.length - this.answerLetters.length + index].text.value === letter);
    }
  }

  get isAtStart() {
    return this.index === 0;
  }

  get isAtEnd() {
    return this.index > 0 && this.index === this.row.entries.length - 1;
  }

  get isWholeRow() {
    return this.row.entries.length === 1;
  }

  get answerFillsGrid() {
    return !this.row.entries.every((entry) => entry.answer.value) ||
      this.row.entries.reduce((length, entry) => length + entry.answerLetters.length, 0) === this.row.cells.length
  }
}

export class BloomEntry extends BaseEntry implements BloomEntrySignature {
  color: Color;

  constructor({color, ...properties}: BaseEntryProperties & BloomEntrySignature) {
    super(properties);
    this.color = color;
    this.autoMarkFilled()
  }
  
  get bloom(): Bloom {
    return this.game.blooms.getOrCreate(this);
  }

  get label() {
    return this.isOrderSecret ? '' : `${this.color[0].toUpperCase()}${this.number}`;
  }

  get cells() {
    return this.bloom.cells;
  }

  get possibleCells() {
    if (bloomClueOrdering.value === 'gridOrder') {
      return this.cells;
    }
    return [...this.game.blooms].filter(({color}) => color === this.color).flatMap((bloom) => bloom.cells);
  }

  get answerFillsGrid() {
    return this.answerLetters.length === this.game.dimension?.cellsPerBloom
  }

  get answerCompatibleWithGridLetters() {
    const { cellsPerBloom } = this.game.dimension;
    const answerMatchesGrid = (offset: number, reverseOrder: boolean) => {
      return range(0, cellsPerBloom).every((index) => {
        const cell = getAdjacentCell(this.cells[index], offset, 'bloom', true);
        const cellText = cell?.text.value;
        const answerLetterIndex = reverseOrder ? (cellsPerBloom - index) % cellsPerBloom : index;
        const answerText = this.answerLetters[answerLetterIndex];
        return !cellText || !answerText || cellText === answerText;
      });
    };
    return range(0, cellsPerBloom).some((offset) =>
      answerMatchesGrid(offset, false) || answerMatchesGrid(offset, true)
    );
  }

  get isOrderSecret(): boolean {
    return isBloom(this) && this.game.bloomOrderIsSecret
  }

  get isSelected(): boolean {
    if(this.isOrderSecret) {
      return !!this.game.cluesSelection.isSelected(this);
    } else {
      return super.isSelected;
    }
  }

  get isPossiblySelected(): boolean {
    if(this.isOrderSecret) {
      const selectedClue = this.game.cluesSelection.selected()
      const selectedBloom = this.game.blooms.selected()
      return (
        !!selectedClue &&
        isBloom(selectedClue) &&
        this.color === selectedClue.color
      ) || (
        !!selectedBloom &&
        this.color === selectedBloom.color
      )
    } else {
      return super.isPossiblySelected
    }
  }

  select() {
    this.game.entries.select(this);
    this.game.rows.deselect();
    if (!this.isOrderSecret) {
      this.game.blooms.select(this.bloom);
      ensureSelectedCellContained(this);
    }
  }
}

function ensureSelectedCellContained(
  entry: Entry,
  getNewSelectedCellIndex: ((selectedCell?: Cell, contained?: boolean) => number | undefined) = (selectedCell, contained) => {
    if(!contained) {
      return 0;
    }
  }
) {
  const { game, possibleCells } = entry;
  const selectedCell = game.cells.selected();
  const contained = selectedCell && possibleCells.includes(selectedCell);
  const newSelectedCellIndex = getNewSelectedCellIndex(selectedCell, contained);
  if(typeof newSelectedCellIndex !== 'undefined') {
    game.cells.select(possibleCells.at(newSelectedCellIndex));
  }
}

export type Entry = RowEntry | BloomEntry;

export function answerLetters(answer: string) {
  return answer
    .replace(/[^a-z]/igm, '')
    .toLocaleUpperCase()
    .split('');
}
