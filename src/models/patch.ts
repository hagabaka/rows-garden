import { PuzzleProperties } from "./puzzle";

const puzzlePatches: (Pick<PuzzleProperties, 'title' | 'author'> & Partial<PuzzleProperties>)[] = [
  {
    title: 'Garden Party Year 7, Number 5',
    author: 'Michael Blake',
    shadedCells: [
      'E7',
      'E8',
      'E9',
      'E10',
      'E11',
      'E12',
      'E13',
      'E14',
      'E15',
      'F7',
      'F15',
      'G7',
      'G15',
      'H7',
      'H15',
      'I7',
      'I8',
      'I9',
      'I10',
      'I11',
      'I12',
      'I13',
      'I14',
      'I15',
    ],
  }
]

export function patchPuzzle(puzzle: PuzzleProperties): PuzzleProperties {
  for (const patch of puzzlePatches) {
    if (puzzle.title === patch.title && puzzle.author === patch.author) {
      return {
       ...puzzle,
       ...patch,
      }
    }
  }
  return puzzle;
}