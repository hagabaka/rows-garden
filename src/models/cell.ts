import { Row } from "./row";
import { Bloom } from "./bloom";
import { Orientation, orientations } from './geometry';
import { Point, Polygon, Rectangle } from '@mathigon/euclid';
import { computed, ref, unref, watch } from "vue";
import { Game, GameElement } from "./game";
import { Entry } from "./entry";
import mapValues from "just-map-values";
import pick from "just-pick";

export interface CellSignature {
  columnIndex: number;
  rowIndex: number;
}

export interface CellState {
  text: string;
  incorrect: boolean;
  revealed: boolean;
}

export class Cell implements CellSignature, GameElement {
  columnIndex: number;
  rowIndex: number;
  game: Game;
  text = ref<string>();
  incorrect = ref(false);
  revealed = ref(false);
  filled = computed(() => !!this.text.value);
  correct = computed(() => this.answer && this.text.value === this.answer);

  constructor({ columnIndex, rowIndex, game }: CellSignature & GameElement) {
    this.columnIndex = columnIndex;
    this.rowIndex = rowIndex;
    this.game = game;
    watch(this.text, () => {
      this.incorrect.value = false;
      this.revealed.value = false;
    }, {
      flush: 'sync'
    });
  }

  get row(): Row {
    return this.game.rows.getOrCreate({index: this.rowIndex});
  }

  get bloom(): Bloom {
    return Bloom.containing(this, this.game);
  }

  get possibleEntries(): Entry[] {
    return [
      ...this.row.entries,
      this.bloom.entry,
    ];
  }

  get orientation(): Orientation {
    return orientations[(this.rowIndex % orientations.length + this.columnIndex % orientations.length) % orientations.length];
  }

  get triangle(): Polygon {
    return this.game.cellTriangle
      .rotate(this.orientation === 'down' ? Math.PI : 0)
      .translate(new Point(
        (this.columnIndex + 1) * this.game.cellWidth / 2,
        this.orientation === 'up'
          ? (this.rowIndex + 2 / 3) * this.game.cellHeight
          : (this.rowIndex + 1 / 3) * this.game.cellHeight
      ))
  }

  get box(): Rectangle {
    return Rectangle.aroundPoints(this.triangle.points);
  }

  get width() { 
    return this.game.cellWidth;
  }

  get height() { 
    return this.game.cellHeight;
  }

  get answer(): string | undefined {
    const indexInRow = this.row.cells.indexOf(this);
    return this.row.answerLetters[indexInRow];
  }

  get selected() {
    return this.game.cells.isSelected(this);
  }

  get isShaded(): boolean {
    return !!this.game.puzzle?.isHighlighted(this, "shade")
  }

  get isCircled(): boolean {
    return !!this.game.puzzle?.isHighlighted(this, "circle")
  }

  reveal() {
    if(this.text.value !== this.answer) {
      this.text.value = this.answer;
      this.revealed.value = true;
    }
  }

  check() {
    this.incorrect.value = this.filled.value && !this.correct.value;
  }

  setText(text: string) {
    this.text.value = text;
  }

  toJSON() {
    const states = pick(this, ['text', 'incorrect', 'revealed']);
    return mapValues(states, unref<unknown>);
  }

  loadJSON(state: CellState) {
    this.text.value = state.text;
    this.incorrect.value = state.incorrect;
    this.revealed.value = state.revealed;
  }
}
