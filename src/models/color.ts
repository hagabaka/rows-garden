export const colors = ['light', 'medium', 'dark'] as const;

export type Color = typeof colors[number];

export type Palette = Record<Color, string>;