import range from "just-range";
import { Cell, CellSignature } from "./cell";
import { Game, GameElement } from "./game";
import { isRow, RowEntry } from "./entry";
import { Circle, Point, Polygon, Rectangle } from "@mathigon/euclid";

export interface RowSignature {
  index: number;
}

export class Row implements RowSignature, GameElement {
  index: number;
  game: Game;

  constructor({index, game}: RowSignature & GameElement) {
    this.index = index;
    this.game = game;
  }

  get label() {
    return getLabel(this.index);
  }

  get cells(): Cell[] {
    let cellSignatures: CellSignature[];
    if(this.index === 0 || this.index === this.game.dimension.rowsPerColumn - 1) {
      cellSignatures = this.getCellsEdgeRow(this.index);
    } else {
      cellSignatures = this.getCellsInnerRow(this.index);
    }
    return cellSignatures.map(signature => this.game.cells.getOrCreate(signature));
  }

  get outline(): Polygon[] {
    return Polygon.union(...this.cells.map(cell => cell.triangle));
  }

  get entries(): RowEntry[] {
    return [...this.game.entries].filter(entry => isRow(entry) && entry.rowIndex === this.index) as RowEntry[];
  }

  get answerLetters(): string {
    return this.entries.map(({answer}) => answer.value).join('').replace(/[^a-z]/ig, '');
  }

  get isSelected(): boolean {
    const { rows, entries } = this.game
    const selectedRow = rows.selected()
    const selectedEntry = entries.selected()
    return (
      !!selectedRow && selectedRow.index === this.index ||
      this.entries.length === 1 && !!selectedEntry && isRow(selectedEntry) && selectedEntry.rowIndex === this.index
    )
  }

  get isPossiblySelected(): boolean {
    const selectedEntry = this.game.entries.selected()
    return !!selectedEntry && isRow(selectedEntry) && selectedEntry.rowIndex === this.index
  }

  get top(): number {
    return this.index * this.game.cellHeight;
  }

  get rule(): Rectangle {
    return new Rectangle(
      new Point(-this.game.rowLabelRadius, this.top + (this.game.cellHeight - this.game.rowRuleHeight) / 2),
      this.game.rowLabelRadius + this.game.gridWidth,
      this.game.rowRuleHeight,
    );
  }

  get circle(): Circle {
    return new Circle(
      new Point(-this.game.rowLabelRadius, this.top + this.game.cellHeight / 2),
      this.game.rowLabelRadius,
    );
  }

  getCellsEdgeRow(rowIndex: number) {
    return range(1, this.game.dimension.bloomsPerRow, 2).flatMap((bloomIndex) =>
      range(0, 3).map(offset =>
        ({columnIndex: bloomIndex * this.game.dimension.columnsPerBloom + offset, rowIndex}))
    );
  }

  getCellsInnerRow(rowIndex: number) {
    return range(0, this.game.dimension.columnsPerRow).map(columnIndex => ({columnIndex, rowIndex}));
  }
}

export function getLabel(rowIndex: number) {
  return String.fromCharCode('A'.charCodeAt(0) + rowIndex);
}