import { CellSignature } from "./cell"
import { Dimension } from "./dimension"
import { answerLetters } from "./entry"
import { EntryProperties, PuzzleProperties } from "./puzzle"

export type HighlightType = 'shade' | 'circle'
export type HighlightCells = Record<HighlightType, CellSignature[]>
export function getHighlightedCells(
  { shadedCells, rows }: Pick<PuzzleProperties, "shadedCells" | "rows">,
  { rowsPerColumn }: Pick<Dimension, 'rowsPerColumn'>,
): HighlightCells {
  const shade = getHighlightedCellsFromRows({ rows }, 'highlight')
  const circle = getHighlightedCellsFromRows({ rows }, 'circle')
  if (shade.length >= 0 || circle.length >= 0) {
    return {
      shade,
      circle,
    }
  } else {
    return {
      shade: getShadedCellsFromCellNames({ shadedCells }, { rowsPerColumn }),
      circle: [],
    }
  }
}

function getShadedCellsFromCellNames(
  { shadedCells }: Pick<PuzzleProperties, "shadedCells">,
  { rowsPerColumn }: Pick<Dimension, 'rowsPerColumn'>,
): CellSignature[]{
  const firstRowLabel = 'A'
  const lastRowLabel = String.fromCharCode('A'.charCodeAt(0) + rowsPerColumn - 1)
  const pattern = new RegExp(`^([${firstRowLabel}-${lastRowLabel}])([1-9][0-9]*)$`)
  return shadedCells?.flatMap((cellName) => {
    const match = cellName.match(pattern)
    if (!match) {
      return []
    } else {
      return {
        rowIndex: match[1].charCodeAt(0) - firstRowLabel.charCodeAt(0),
        columnIndex: parseInt(match[2]) - 1,
      }
    }
  }) ?? []
}

function getHighlightedCellsFromRows(
  { rows }: Pick<PuzzleProperties, "rows">,
  key: keyof EntryProperties<string>
): CellSignature[] {
  const cellSignatures = rows.reduce<CellSignature[]>((previousCellSignatures, entries, rowIndex) => {
    const { cellSignatures } = entries.reduce<{
      index: number
      cellSignatures: CellSignature[]
    }>(({ index, cellSignatures }, { [key]: highlight, answer }) => {
      const startIndex = index
      index += answerLetters(answer).length
      if (highlight) {
        const symbols = highlight.replace(/[^.+]/g, '').split('')
        return {
          index,
          cellSignatures: [
            ...cellSignatures,
            ...symbols.flatMap((symbol, offset) => symbol === '+'
              ? {
                rowIndex,
                columnIndex: startIndex + offset,
              }
              : []
            )
          ]
        }
      } else {
        return {
          index,
          cellSignatures,
        }
      }
    }, { index: 0, cellSignatures: [] })
    return [
      ...previousCellSignatures,
      ...cellSignatures,
    ]
  }, [])
  return cellSignatures
}
