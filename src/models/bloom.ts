import { Cell } from "./cell";
import { Color, colors } from "./color";
import { Polygon } from "@mathigon/euclid";
import { Game, GameElement } from "./game";
import range from "just-range";
import { BloomEntry, isBloom } from "./entry";

export interface BloomSignature {
  color: Color;
  index: number;
}

export class Bloom implements BloomSignature {
  color: Color;
  index: number;
  game: Game;

  constructor({color, index, game}: BloomSignature & GameElement) {
    this.color = color;
    this.index = index;
    this.game = game;
  }

  /*   0   3   6   9   12  15  18
     0     M0      M1      M2
     1 L0  M0  L1  M1  L2  M2  L3
     2 L0  D0  L1  D1  L2  D2  L3
     3 M3  D0  M4  D1  M5  D2  M6
     4 M3  L4  M4  L5  M5  L6  M6
     5 D3  L4  D4  L5  D5  L6  D6
     6 D3  M7  D4  M8  D5  M9  D6
     7 L7  M7  L8  M8  L9  M9  L10
     8 L7  D7  L8  D8  L9  D9  L10
     9 M10 D7  M11 D8  M12 D9  M13
    10 M10 L11 M11 L12 M12 L13 M13
    11     L11     L12     L13
  */

  get entry(): BloomEntry {
    return this.game.entries.get(this) as BloomEntry;
  }

  get number() {
    return this.index + 1
  }

  get leftColumnIndex(): number {
    /*
      i  L  M  D
      0  0  3  3
      1  6  9  9
      2  12 15 15
      3  18 0  0
      4  3  6  6
      5  9  12 12
      9  12 15 15
    */
    return ([0, 3, 3][colors.indexOf(this.color)] + this.index * 6) % 21;
  }

  get topRowIndex(): number {
    /*
    i   L  M  D
    0   1  0  2
    1   1  0  2
    2   1  0  2
    3   1  3  5
    4   4  3  5
    5   4  3  5
    6   4  3  5
    7   7  6  8
    8   7  6  8
    9   7  6  8
    10  7  9  11
    11  10 9  11
    12  10 9  11
    13  10 9  11
    */
    return (
      this.index % 7 < (this.color === 'light' ? 4 : 3)
      ? [1, 0, 2]
      : [4, 3, 5]
    )[colors.indexOf(this.color)] + 6 * Math.floor(this.index / 7);
  }

  static containing(cell: Cell, game: Game): Bloom {
    const leftColumnIndex = Math.floor(cell.columnIndex / 3) * 3;
    /*
    0  1  2  3  4  5  6  7  8  9  10 11
    DM LM LD MD ML DL DM LM LD MD ML LD
    */
    const color = colors[
      [[2, 1], [0, 1], [0, 2], [1, 2], [1, 0], [2, 0]]
      [cell.rowIndex % 6]
      [leftColumnIndex % 2]
    ];
    /*
         0  3  6  9 12 15 18
    0       0     1     2
    1    0  0  1  1  2  2  3
    2    0  0  1  1  2  2  3
    3    3  0  4  1  5  2  6
    4    3  4  4  5  5  6  6
    5    3  4  4  5  5  6  6
    6    3  7  4  8  5  9  6
    7    7  7  8  8  9  9 10
    8    7  7  8  8  9  9 10
    9   10  7 11  8 12  9 13
    10  10 11 11 12 12 13 13
    11     11    12    13
    */
    const even = leftColumnIndex % 2 === 0;
    const pattern = even ? [-4, 0, 0, 3, 3, 3] : [0, 0, 0, 0, 4, 4];
    const columnAdjust = Math.floor(leftColumnIndex / 6);
    const rowAdjust = Math.floor(cell.rowIndex / 6) * 7;
    const index = pattern[cell.rowIndex % pattern.length] + columnAdjust + rowAdjust;
    return game.blooms.getOrCreate({
      color,
      index,
    });
  }

  get hexagon(): Polygon {
    return Polygon.union(...this.cells.map(cell => cell.triangle))[0];
  }

  get radius(): number {
    return this.game.cellWidth;
  }

  get cells(): Cell[] {
    if (!this.game.isOpen()) {
      return []
    }
    const { rowsPerBloom, columnsPerBloom } = this.game.dimension
    return range(0, rowsPerBloom).flatMap(rowOffset => 
      range(0, columnsPerBloom).flatMap(columnOffset =>
        this.game.cells.getOrCreate({
          columnIndex: this.leftColumnIndex + (rowOffset % 2 === 0 ? columnOffset : columnsPerBloom - columnOffset - 1),
          rowIndex: this.topRowIndex + rowOffset,
        })
    ));
  }

  get isSelected(): boolean {
    return !!this.game.blooms.isSelected(this)
  }

  get isPossiblySelected(): boolean {
    const selectedClue = this.game.cluesSelection.selected()
    return !!selectedClue &&
      this.game.bloomOrderIsSecret &&
      isBloom(selectedClue) &&
      selectedClue.color === this.color
  }
}