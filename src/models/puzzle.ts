import { Color } from "./color";
import { Dimension, dimensionOptions } from "./dimension";
import { GameElement, Game } from "./game";
import { ref, Ref, shallowRef } from "vue";
import range from "just-range";
import { Cell, CellSignature } from "./cell";
import { patchPuzzle } from "./patch";
import { getHighlightedCells, HighlightCells, HighlightType } from "./highlight";
import { mapToObj, mapValues } from "remeda";

export interface PuzzleProperties {
  title?: string
  author?: string
  notes?: string
  created?: string
  url?: string
  copyright?: string
  rows: EntryProperties<string>[][];
  dark: EntryProperties<string>[];
  medium: EntryProperties<string>[];
  light: EntryProperties<string>[];
  editing?: boolean;
  draft?: boolean;
  shadedCells?: string[]
  dimension?: Dimension
}

export function makePuzzleProperties(dimension = dimensionOptions.standard): PuzzleProperties {
  const appTitle = import.meta.env.VITE_APP_TITLE;
  const canonicalUrl = import.meta.env.VITE_CANONICAL_URL;
  const { darkBlooms, lightBlooms, mediumBlooms, rowsPerColumn } = dimension;
  return {
    title: 'Untitled',
    author: 'Anonymous',
    notes: `Created with ${appTitle} ${canonicalUrl}`,
    created: new Date().toISOString().replace(/T.*/, ''),
    url: '',
    copyright: '',
    rows: range(0, rowsPerColumn).map(() => [makeEntryProperties()]),
    dark: range(0, darkBlooms).map(makeEntryProperties),
    medium: range(0, mediumBlooms).map(makeEntryProperties),
    light: range(0, lightBlooms).map(makeEntryProperties),
    dimension,
  }
}

export interface EntryProperties<Value> {
  answer: Value;
  clue: Value;
  highlight?: Value;
  circle?: Value;
}

function makeEntryProperties() {
  return {
    answer: '',
    clue: '',
  }
}

export class Puzzle implements GameElement {
  title: Ref<string | undefined>
  author: Ref<string | undefined>
  notes: Ref<string | undefined>
  created: Ref<string | undefined>
  url: Ref<string | undefined>
  copyright?: Ref<string | undefined>
  rows: Ref<EntryProperties<Ref<string>>[]>[];
  dark: EntryProperties<Ref<string>>[];
  medium: EntryProperties<Ref<string>>[];
  light: EntryProperties<Ref<string>>[];
  editing: Ref<boolean>;
  game: Game;
  draft: Ref<boolean>;
  highlightedCellSignatures: Ref<HighlightCells>;

  constructor({ game, ...properties }: PuzzleProperties & GameElement) {
    const { title, author, notes, created, url, copyright, rows, dark, medium, light, editing, draft, shadedCells } = patchPuzzle(properties);
    this.title = ref(title);
    this.author = ref(author);
    this.notes = ref(notes);
    this.created = ref(created);
    this.url = ref(url);
    this.copyright = ref(copyright);
    this.game = game;
    this.rows = rows.map((entries) => shallowRef(entries.map(({answer, clue}) => ({
      answer: ref(answer),
      clue: ref(clue),
    }))));
    this.dark = dark.map(({ answer, clue }) => ({
      answer: ref(answer),
      clue: ref(clue),
    }));
    this.medium = medium.map(({ answer, clue }) => ({
      answer: ref(answer),
      clue: ref(clue),
    }));
    this.light = light.map(({ answer, clue }) => ({
      answer: ref(answer),
      clue: ref(clue),
    }));
    this.editing = ref(!!editing);
    this.draft = ref(!!draft);
    this.highlightedCellSignatures = ref(getHighlightedCells({ shadedCells, rows }, game.dimension))
    this.entries;
  }

  get entries() {
    const rowEntry = (rowIndex: number) =>
      (entryProperties: EntryProperties<Ref<string>>, index: number) =>
      this.game.entries.getOrCreate({
        rowIndex,
        index,
        game: this.game,
        ...entryProperties,
      })
    const bloomEntry = (color: Color) =>
      (entryProperties: EntryProperties<Ref<string>>, index: number) =>
      this.game.entries.getOrCreate({
        color,
        index,
        game: this.game,
        ...entryProperties,
      })
    return {
      rows: this.rows.map((entriesProperties, rowIndex) =>
        entriesProperties.value.map(rowEntry(rowIndex)),
      ),
      dark: this.dark.map(bloomEntry('dark')),
      light: this.light.map(bloomEntry('light')),
      medium: this.medium.map(bloomEntry('medium')),
    }

  }

  get highlightedCells() {
    return mapValues(
      this.highlightedCellSignatures.value,
      (signatures, type) => (signatures.flatMap(
        (signature) => this.game.cells.get(signature) || []
      ))
    )
  }

  isHighlighted(cell: Cell, type: HighlightType): boolean {
    return this.highlightedCells[type].includes(cell)
  }

  addEntry(rowIndex: number, index: number) {
    const entry = {
      answer: ref(''),
      clue: ref(''),
    }
    this.game.entries.getOrCreate({
      rowIndex,
      index,
      game: this.game,
      ...entry,
    })
    this.rows[rowIndex].value = [
      ...this.rows[rowIndex].value.slice(0, index + 1),
      entry,
      ...this.rows[rowIndex].value.slice(index + 1),
    ]
  }

  removeEntry(rowIndex: number, index: number) {
    if (this.rows[rowIndex].value.length === 1) {
      return;
    }
    this.rows[rowIndex].value = [
      ...this.rows[rowIndex].value.slice(0, index),
      ...this.rows[rowIndex].value.slice(index + 1),
    ];
    this.game.entries.delete({ rowIndex, index });
  }

  toJSON(): PuzzleProperties {
    return {
      title: this.title.value,
      author: this.author.value,
      notes: this.notes.value,
      created: this.created.value,
      url: this.url.value,
      rows: this.rows.map((entries) => entries.value.map(({answer, clue}) => ({
        answer: answer.value,
        clue: clue.value,
      }))),
      dark: this.dark.map(({ answer, clue }) => ({
        answer: answer.value,
        clue: clue.value,
      })),
      light: this.light.map(({ answer, clue }) => ({
        answer: answer.value,
        clue: clue.value,
      })),
      medium: this.medium.map(({ answer, clue }) => ({
        answer: answer.value,
        clue: clue.value,
      })),
      editing: this.editing.value,
      draft: this.draft.value,
    }
  }

  async save() {
    const { createRgz } = await import('./file');
    const { editing, ...json } = this.toJSON();
    const blob = await createRgz(json);
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = `${this.title.value}.rgz`;
    a.click();
  }
}
