import { Polygon } from "@mathigon/euclid";
import pick from "just-pick";
import { ref, watchEffect } from "vue";
import { GameElement, Game } from "./game";
import { CellState } from "./cell";

export interface GridState {
  cells: Record<string, CellState>;
}

export class Grid implements GameElement {
  game: Game;
  status = ref<'in-progress' | 'filled' | 'completed'>('in-progress');

  constructor({game, state}: GameElement & {state?: GridState}) {
    this.game = game;
    this.status = ref('in-progress');
    const { rowsPerColumn } = this.game.dimension
    for(let index = 0; index < rowsPerColumn; index++) {
      const row = this.game.rows.getOrCreate({index});
      for(const cell of row.cells) {
        cell.bloom;
      }
    }
    if(state) {
      this.loadJSON(state);
    }
    watchEffect(() => {
      if (this.game.isOpen()) {
        const cells = [...this.game.cells];
        if(cells.every(cell => cell.correct.value)) {
          this.status.value = 'completed';
        } else if(cells.every(cell => cell.filled.value)) {
          this.status.value = 'filled';
        }
      }
    })
  }

  get width(): number {
    return this.game.gridWidth;
  }

  get height(): number {
    return this.game.gridHeight;
  }

  get outline() {
    return Polygon.union(...[...this.game.blooms].flatMap(bloom => bloom.hexagon))[0];
  }

  toJSON() {
    return pick(this.game, ['cells']);
  }

  loadJSON({cells}: GridState) {
    this.game.cells.loadJSON(
      cells,
      key => {
        const [columnIndex, rowIndex] = key.split(',').map(Number)
        return {
          columnIndex, rowIndex,
        }
      },
      (cell, state) => cell.loadJSON(state)
    )
  }
}