import { ref } from "vue";
import { Registry } from "./registry";

export class Selection<Signature,  Item extends Signature, Properties extends Signature> {
  registry: Registry<Signature, Item, Properties>;
  selectedItemSignature = ref<Signature>();
  constructor(registry: Registry<Signature, Item, Properties>) {
    this.registry = registry;
  }

  isSelected(signature: Signature) {
    return this.selectedItemSignature.value && this.registry.getKey(signature) === this.registry.getKey(this.selectedItemSignature.value);
  }

  selected() {
    return this.selectedItemSignature.value && this.registry.get(this.selectedItemSignature.value);
  }

  select(item: Item | undefined) {
    this.selectedItemSignature.value = item;
  }

  deselect() {
    this.selectedItemSignature.value = undefined;
  }
}