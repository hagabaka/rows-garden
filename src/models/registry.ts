import { ref, shallowRef } from "vue";

type GetKey<Signature> = (signature: Signature) => string;
type CreateItem<Properties, Item> = (properties: Properties) => Item;

export class Registry<Signature, Item extends Signature, Properties extends Signature = Signature> {
  map = shallowRef(new Map<string, Item>());
  getKey: GetKey<Signature>;
  createItem: CreateItem<Properties, Item>;
  selectedItemSignature = ref<Signature>();
  selection: Selection<Signature, Item, Properties>
  
  constructor(getKey: GetKey<Signature>, createItem: CreateItem<Properties, Item>) {
    this.getKey = getKey;
    this.createItem = createItem;
    this.selection = new Selection(this);
  }

  getOrCreate(properties: Properties): Item {
    let item = this.get(properties);
    if(!item) {
      item = this.createItem(properties);
      this.update(map => {
        if (item) {
          map.set(this.getKey(properties), item)
        }
      });
    }
    return item;
  }

  get(signature: Signature): Item | undefined {
    return this.map.value.get(this.getKey(signature));
  }

  delete(signature: Signature) {
    this.update(map => map.delete(this.getKey(signature)))
  }

  update(change: (map: Map<string, Item>) => void) {
    change(this.map.value);
    this.map.value = new Map([...this.map.value.entries()]);
  }

  isSelected(item: Signature) {
    return this.selection.isSelected(item);
  }

  selected() {
    return this.selection.selected();
  }

  select(item: Item | undefined) {
    return this.selection.select(item);
  }

  deselect() {
    return this.selection.deselect();
  }

  [Symbol.iterator]() {
    return this.map.value.values();
  }

  toJSON() {
    return Object.fromEntries(this.map.value);
  }

  loadJSON<State>(
    states: Record<string, State>,
    parseSignature: (key: string) => Signature,
    loadItemState: (item: Item, state: State) => void,
  ) {
    for(const [key, state] of Object.entries(states)) {
      const item = this.get(parseSignature(key));
      if(item) {
        loadItemState(item, state);
      }
    }
  }
}

export class Selection<Signature,  Item extends Signature, Properties extends Signature> {
  registry: Registry<Signature, Item, Properties>;
  selectedItemSignature = ref<Signature>();
  constructor(registry: Registry<Signature, Item, Properties>) {
    this.registry = registry;
  }

  isSelected(signature: Signature) {
    return this.selectedItemSignature.value && this.registry.getKey(signature) === this.registry.getKey(this.selectedItemSignature.value);
  }

  selected() {
    return this.selectedItemSignature.value && this.registry.get(this.selectedItemSignature.value);
  }

  select(item: Item | undefined) {
    this.selectedItemSignature.value = item;
  }

  deselect() {
    this.selectedItemSignature.value = undefined;
  }
}