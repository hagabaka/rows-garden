import { useAsyncState, useFetch } from "@vueuse/core";
import { BlobReader, ERR_EOCDR_NOT_FOUND, TextWriter, ZipReader, ZipWriter, TextReader, BlobWriter } from "@zip.js/zip.js";
import { useQuasar } from "quasar";
import { ref, computed } from "vue";
import { parse, stringify } from "yaml";
import { PuzzleProperties } from "./puzzle";

export function useLoader<Arg>(
  loadPuzzle: (parameter: Arg) => Promise<PuzzleProperties[]>,
  onLoad: (puzzle: PuzzleProperties) => void,
) {
  const quasar = useQuasar();
  const asyncState = useAsyncState(loadPuzzle, [], {
    immediate: false,
    onError: console.error,
  });
  const otherError = ref<Error>();
  const error = computed(() => asyncState.error.value);
  return {
    isError: computed(() => !!error.value),
    errorMessage: computed(() => error.value instanceof Error ? error.value.message : ''),
    loading: computed(() => asyncState.isLoading.value),
    load: async (arg: Arg) => {
      const puzzles = await asyncState.execute(undefined, arg);
      if(puzzles.length > 1) {
        const items = puzzles.map((puzzle, index) => ({
          label: puzzle.title || (index + 1).toString(),
          value: index.toString(),
        }));
        quasar.dialog({
          message: 'Select puzzle',
          options: {
            type: 'radio',
            model: items[0].value,
            items,
          },
        }).onOk(index => {
          onLoad(puzzles[index]);
        });
      } else if(puzzles.length === 1) {
        onLoad(puzzles[0])
      } else {
        otherError.value = new Error('The file puzzle contains no puzzles');
      }
    },
  }
}

export async function parseFile(blob: Blob): Promise<PuzzleProperties[]> {
  try {
    return await parseRgzFile(blob);
  } catch(error) {
    if([ERR_EOCDR_NOT_FOUND].includes((error as Error).message)) {
      return [await parseRgFile(blob)];
    } else {
      throw error;
    }
  }
}

export async function loadFromUrl(url: string, retryThroughProxy = true): Promise<PuzzleProperties[]> {
  const blob = (await useFetch(url).blob()).data;
  if(blob.value) {
    return await parseFile(blob.value);
  } else if(retryThroughProxy) {
    return await loadFromUrl(`${import.meta.env.VITE_CORS_ANYWHERE_URL}/${url}`, false);
  } else {
    throw new Error('Failed to fetch file');
  }
}

export async function parseRgzFile(blob: Blob): Promise<PuzzleProperties[]> {
  const zipReader = new ZipReader(new BlobReader(blob));
  const entries = await zipReader.getEntries();
  const puzzles = await Promise.all(entries.map(async entry => {
    if(entry.getData) {
      const textWriter = new TextWriter('utf-8');
      await entry.getData(textWriter);
      const text = await textWriter.getData();
      return await parseRg(text);
    } else {
      throw new Error('The file is not supported.')
    }
  }));
  return puzzles.flat();
}

export async function parseRgFile(blob: Blob): Promise<PuzzleProperties> {
  return await parseRg(await blob.text());
}

async function parseRg(
  text: string,
  retries: ((text: string) => string)[] = [
    (text) => text.replace(/^( *[^:]+:)([^ ].*)$/gm, '$1 $2'),
    // insert missing space after first colon,
    (text) => text.replace(/^( *[^:]+: +)((?![>|]).+)$/gm, (_, key, value) => `${key}'${value.replace(/'/g, "''")}'`),
    // surround value with quotes and escape inner quotes
  ]
): Promise<PuzzleProperties> {
  try {
    const { draft, editing, ...parsed }: Omit<PuzzleProperties, 'draft' | 'editing'> & {
      draft?: string | boolean
      editing?: string | boolean
    } = await parse(text, {strict: false})
    function fixBoolean(value?: string | boolean): boolean | undefined {
      return value === 'true' || value === true
    }
    return {
      ...parsed,
      draft: fixBoolean(draft),
      editing: fixBoolean(editing),
    }
  } catch (error) {
    if(retries.length > 0) {
      return await parseRg(retries[0](text), retries.slice(1));
    } else {
      throw error;
    }
  }
}

async function createRg(puzzle: PuzzleProperties) {
  return stringify(puzzle)
}

export async function createRgz(puzzle: PuzzleProperties) {
  const blobWriter = new BlobWriter("application/zip");
  const zipWriter = new ZipWriter(blobWriter);
  const textReader = new TextReader(await createRg(puzzle));
  await zipWriter.add('puzzle.rg', textReader);
  await zipWriter.close();
  return blobWriter.getData();
}