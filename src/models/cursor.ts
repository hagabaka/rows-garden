import { Game } from "./game";
import { computed, ref, watch } from 'vue';
import { isBloom, isRow } from "./entry";
import { clamp } from "@vueuse/core";
import { skipFilledCells } from "./settings";
import { Cell } from "./cell";

export type CursorMode = 'row' | 'bloom';
export enum CursorDirection {
  forward = +1,
  reverse = -1,
}

export class Cursor {
  game: Game;
  direction = ref<CursorDirection>(CursorDirection.forward);
  currentCell = computed(() => this.game.cells.selected());
  currentEntry = computed(() => this.game.entries.selected());
  currentRow = computed(() => this.game.rows.selected());
  currentBloom = computed(() => this.game.blooms.selected());
  mode = computed<CursorMode | undefined>(() =>
    this.currentRow.value || this.currentEntry.value && isRow(this.currentEntry.value) ? 'row' :
    this.currentBloom.value || this.currentEntry.value && isBloom(this.currentEntry.value) ? 'bloom' :
    undefined
  )
  currentEntryCells = computed(() => this.mode.value ? this.currentCell.value?.[this.mode.value].cells ?? [] : []);
  nextCell = computed(() => {
    let result: Cell | undefined;
    let offset = this.direction.value;
    do {
      const next = this.getAdjacentCell(offset);
      if(next === result) {
        break;
      }
      result = next;
      if(result === this.currentCell.value) {
        break;
      }
      offset += this.direction.value;
    } while(skipFilledCells.value && result?.text.value);
    return result;
  });
  previousCell = computed(() => this.getAdjacentCell(- this.direction.value));
  inRightAlignMode = computed(() => this.direction.value === CursorDirection.reverse && this.mode.value === 'row');
  rightAlignedModeLetters: string[] = [];
  acceptInput = ref(true);

  constructor(game: Game) {
    this.game = game;
    watch([this.mode, this.currentCell], ([mode, currentCell]) => {
      const { columnsPerRow } = this.game.dimension;
      if(currentCell) {
        if(mode === 'row') {
          this.direction.value = currentCell.columnIndex === columnsPerRow - 1
            ? CursorDirection.reverse
            : CursorDirection.forward;
        } else {
          this.direction.value = CursorDirection.forward;
        }
      }
    })
    watch(this.inRightAlignMode, inRightAlignMode => {
      if(!inRightAlignMode) {
        this.rightAlignedModeLetters = [];
      }
    });
  }

  onInput(letter: string) {
    if(!this.acceptInput.value) return;
    if(this.inRightAlignMode.value) {
      if(!this.currentCell.value) {
        return;
      }
      this.rightAlignedModeLetters.push(letter);
      for(const [index, letter] of this.rightAlignedModeLetters.entries()) {
        this.currentCell.value.row.cells.at(index - this.rightAlignedModeLetters.length)?.setText(letter.toUpperCase());
      }
    } else {
      this.currentCell.value?.setText(letter.toUpperCase());
      this.game.cells.select(this.nextCell.value);
    }
  }

  onKeyUp({key}: KeyboardEvent) {
    if (key === 'Delete') {
      this.currentCell.value?.setText('');
    } else if (key === 'Backspace') {
      if(this.inRightAlignMode.value && this.rightAlignedModeLetters.length > 1) {
        if(!this.currentCell.value) {
          return;
        }
        this.currentCell.value.row.cells.at(-this.rightAlignedModeLetters.length)?.setText('');
        this.rightAlignedModeLetters.pop();
        for(const [index, letter] of this.rightAlignedModeLetters.entries()) {
          this.currentCell.value.row.cells.at(index - this.rightAlignedModeLetters.length)?.setText(letter.toUpperCase());
        }
      } else {
        this.currentCell.value?.setText('');
        this.game.cells.select(this.inRightAlignMode.value ? this.nextCell.value : this.previousCell.value);
      }
    } else if(key === 'Enter') {
      this.cycle();
    } else if(key === 'ArrowLeft') {
      this.move(-1, 0); 
    } else if(key === 'ArrowRight') {
      this.move(1, 0); 
    } else if(key === 'ArrowUp') {
      this.move(0, -1);
    } else if(key === 'ArrowDown') {
      this.move(0, 1);
    }
  }

  move(columnDistance: number, rowDistance: number) {
    const { rowsPerColumn, columnsPerRow } = this.game.dimension;
    if(this.currentCell.value) {
      const {rowIndex, columnIndex} = this.currentCell.value;
      const targetCell = this.game.cells.get({
        rowIndex: clamp(rowIndex + rowDistance, 0, rowsPerColumn - 1),
        columnIndex: clamp(columnIndex + columnDistance, 0, columnsPerRow - 1),
      });
      if(targetCell) {
        this.game.cells.select(targetCell);
      }
    }
  }

  getAdjacentCell(offset: number, cycling = this.mode.value === 'bloom') {
    if(!this.currentCell.value || !this.mode.value) {
      return;
    }
    return getAdjacentCell(this.currentCell.value, offset, this.mode.value, cycling);
  }

  cycle() {
    const { columnsPerRow } = this.game.dimension;
    const currentCell = this.currentCell.value;
    if(!currentCell) {
      return;
    }
    if(this.mode.value === 'bloom') {
      if(this.direction.value === CursorDirection.forward) {
        this.direction.value = CursorDirection.reverse;
      } else {
        this.game.entries.select(currentCell.row.entries.at(
          currentCell.columnIndex === columnsPerRow - 1
            ? -1
            : 0
        ))
      }
    } else {
      this.game.rows.deselect()
      this.game.blooms.select(currentCell.bloom)
    }
  }
}

export function getAdjacentCell(cell: Cell, offset: number, mode: CursorMode, cycling = false) {
  const currentEntryCells = cell[mode].cells
  const index = currentEntryCells.indexOf(cell) + offset;
  return currentEntryCells.at(cycling ? index % currentEntryCells.length : clamp(index, 0, currentEntryCells.length - 1));
}