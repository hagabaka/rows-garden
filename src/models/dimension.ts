import { sumBy } from "remeda"
import { PuzzleProperties } from "./puzzle"
import { answerLetters } from "./entry"

interface DimensionOptions {
  rowsPerColumn: number
  columnsPerRow: number
  lightBlooms: number
  darkBlooms: number
  mediumBlooms: number
}

const cellsPerBloom = 6
const rowsPerBloom = 2
const columnsPerBloom = 3

export class Dimension {
  #rowsPerColumn: number
  #columnsPerRow: number
  #lightBlooms: number
  #darkBlooms: number
  #mediumBlooms: number
  constructor({
    rowsPerColumn,
    columnsPerRow,
    lightBlooms,
    darkBlooms,
    mediumBlooms,
  }: DimensionOptions) {
    this.#rowsPerColumn = rowsPerColumn
    this.#columnsPerRow = columnsPerRow
    this.#lightBlooms = lightBlooms
    this.#darkBlooms = darkBlooms
    this.#mediumBlooms = mediumBlooms
  }
  is(dimension: Dimension) {
    return (
      this.rowsPerColumn === dimension.rowsPerColumn &&
      this.columnsPerRow === dimension.columnsPerRow &&
      this.lightBlooms === dimension.lightBlooms &&
      this.darkBlooms === dimension.darkBlooms &&
      this.mediumBlooms === dimension.mediumBlooms
    )
  }
  get rowsPerColumn() {
    return this.#rowsPerColumn
  }
  get columnsPerRow() {
    return this.#columnsPerRow
  }
  get lightBlooms() {
    return this.#lightBlooms
  }
  get mediumBlooms() {
    return this.#mediumBlooms
  }
  get darkBlooms() {
    return this.#darkBlooms
  }
  get bloomsPerRow() {
    return this.#columnsPerRow / columnsPerBloom
  }
  get bloomsPerColumn() {
    return this.#rowsPerColumn / rowsPerBloom
  }
  get cellsPerBloom() {
    return cellsPerBloom
  }
  get rowsPerBloom() {
    return rowsPerBloom
  }
  get columnsPerBloom() {
    return columnsPerBloom
  }
}

export function dimensionFromPuzzle(puzzle: PuzzleProperties) {
  const lightBlooms = puzzle.light.length
  const darkBlooms = puzzle.dark.length
  const mediumBlooms =  puzzle.medium.length
  const rowsPerColumn = puzzle.rows.length
  const columnsPerRow = puzzle.rows.reduce((maxColumns, row) => {
    const columns = sumBy(row, (entry) => answerLetters(entry.answer).length)
    return Math.max(maxColumns, columns)
  }, 0)
  return new Dimension({
    rowsPerColumn,
    columnsPerRow,
    lightBlooms,
    darkBlooms,
    mediumBlooms,
  })
}

export const dimensionOptions = {
  standard: new Dimension({
    rowsPerColumn: 12,
    columnsPerRow: 21,
    lightBlooms: 14,
    darkBlooms: 10,
    mediumBlooms: 14,
  }),
  rosebud: new Dimension({
    rowsPerColumn: 8,
    columnsPerRow: 15,
    lightBlooms: 5,
    mediumBlooms: 7,
    darkBlooms: 5,
  })
}