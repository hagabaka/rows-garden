import { Ref, computed, watch } from 'vue';
import { useStorage, syncRef } from "@vueuse/core";
import { Grid, GridState } from "./grid";
import { Puzzle, PuzzleProperties } from "./puzzle";

export function getState(grid: Grid, puzzle: Puzzle) {
  return JSON.stringify({grid, puzzle});
}

export const storedState = useStorage<string>('state', '');
export function autoSaveState(grid: Ref<Grid | undefined>, puzzle: Ref<Puzzle | undefined>) {
  const state = computed(() => grid.value && puzzle.value
    ? getState(grid.value, puzzle.value)
    : ''
  )
  watch(state, state => {
    if(state) {
      storedState.value = state;
    }
  })
}

export function loadState(): {grid: GridState, puzzle: PuzzleProperties} | undefined {
  if(storedState.value) {
    return JSON.parse(storedState.value);
  }
}