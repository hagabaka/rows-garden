# Rows Garden puzzle solving interface

## Features

* Load RGZ and RG files, supporting files containing multiple puzzles
* Load puzzles from URL with CORS support
* Keyboard navigation
* Options to display answer letter/word count
* Light and dark modes

## Plans

* [ ] Puzzle embedding
* [ ] Puzzle editor
* [ ] Multiplayer
* [ ] PWA to allow installation and registering file handler for RGZ and RG files